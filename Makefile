############################
# Language Drills
# version 0.1.7
# Author: Ionutz Borcoman
# <borco@usa.net>, <borco@borco-ei.eng.hokudai.ac.jp>
############################

all:
	cd src && $(MAKE) all

tarclean:
	cd src && $(MAKE) tarclean

clean:
	cd src && $(MAKE) clean

distclean:
	cd src && $(MAKE) distclean

build:
	cd src && $(MAKE) build

install:
	cd src && $(MAKE) install
