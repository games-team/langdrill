/*
 * ===========================
 * langdrill: Language Drills
 * Version 0.1.5
 * 12/1998
 * ===========================
 *
 * Copyright (C) 1998, Ionutz Borcoman
 * Developed by Ionutz Borcoman <borco@usa.net>, <borco@borco-ei.eng.hokudai.ac.jp>
 *
 * Many thanks to Mario Motta <mmotta@guest.net> for the help given
 * during developing this program.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.hh"

#include <iconv.h>

/////////////////////////////////////////////
//
// strdup_new:
//
//      creates acopy of buff using op new.
//      returns a pointer to the new allocated
//      memory region. use delete to free
//      the memory.
//
/////////////////////////////////////////////
char *strdup_new(char * buff)
{
  char *tmp = NULL;
  if(buff){
    int len = strlen(buff) + 1;
    tmp = new char[len +1];
    // copy buff, including the \0
    memcpy(tmp,buff,len+1);
  }
  return tmp;
}

/////////////////////////////////////////////
// MyException:
/////////////////////////////////////////////
MyException::MyException(int value, char *message)
{
  _value = value;
  _message = strdup_new(message);
}

MyException::~MyException()
{
  delete _message;
}

int
MyException::Value()
{
  return _value;
}

char *
MyException::Message() const
{
  return _message;
}

/////////////////////////////////////////////
//
// MyKey:
//
//      has a private buffer _value. can return
//      a copy of it or change its value.
//      convenient function for int values are
//      also provided.
//
/////////////////////////////////////////////
MyKey::MyKey(char *name, char *value)
{
  _name  = strdup_new(name);
  _value = strdup_new(value);
}

MyKey::MyKey(char *name, int value)
{
  _name  = strdup_new(name);
  _value = new char[MAX_CHARS_FOR_NUMBER];
  sprintf( _value, "%i", value);
}

MyKey::~MyKey()
{
  delete [] _name;
  delete [] _value;
}

char *
MyKey::Get() const
{
  return _value;
}

void
MyKey::Set(char *value)
{
  delete [] _value;
  _value = strdup_new(value);
}

int
MyKey::GetInt()
{
  return atoi(_value);
}

void
MyKey::SetInt(int value)
{
  delete [] _value;
  _value = new char[MAX_CHARS_FOR_NUMBER];
  sprintf( _value, "%i", value);
}

char *
MyKey::Name() const
{
  return _name;
}

/////////////////////////////////////////////
// MySection
/////////////////////////////////////////////
MySection::MySection(char *name)
{
  // initialize an empty GSList
  _number = 0;
  _name   = strdup_new(name);
}

MySection::~MySection()
{
  delete [] _name;

  // delete the keys
  MyKeyListIter li(_list);
  while(li){
    delete li.current();
    li++;
  }
}

MyKey *
MySection::Key(char *name)
{
  MyKey *tmp;

  MyKeyListIter li(_list);
  while(li){
    tmp = li.current();
    if(strcmp(tmp->Name(),name)==0)
      return tmp;
    li++;
  }

//   char message[MAX_CHARS_FOR_MESSAGE];
//   sprintf(message,"MySection::Key - key %s not found", name);
//   throw new MyException(4, message);

  return NULL;
}

void
MySection::Add(char *name)
{
  // check to see if we have this key
  MyKey  *tmp;

  MyKeyListIter li(_list);
  while(li){
    tmp = li.current();
    if(strcmp(tmp->Name(),name)==0)
      return;
    li++;
  }

  tmp = new MyKey( name );
  _list.add(tmp);
  _number++;
}

void
MySection::Remove(char *name)
{
  // check to see if we have this key
  MyKey  *tmp;

  MyKeyListIter li(_list);
  while(li){
    tmp = li.current();
    if(strcmp(tmp->Name(),name)==0){
      // we have found the key
      // we remove it from the list
      _list.remove(tmp);
      // we delete the key data
      delete tmp;
      // decrement number of keys
      _number--;
      return;
    }
    li++;
  }

}

int
MySection::Number()
{
  return _number;
}

char *
MySection::Name() const
{
  return _name;
}

/////////////////////////////////////////////
// MyConfigFile:
/////////////////////////////////////////////
MyConfig::MyConfig(char *file)
{
  // initialize an empty GSList
  _number = 0;
  _name   = strdup_new(file);
}

MyConfig::~MyConfig()
{
  delete [] _name;

  // delete the sections
  MySectionListIter li(_list);
  while(li){
    delete li.current();
    li++;
  }
}

MySection *
MyConfig::Section(char *name)
{
  MySection *tmp;

  MySectionListIter li(_list);
  while(li){
    tmp = li.current();
    if(strcmp(tmp->Name(),name)==0)
      return tmp;
    li++;
  }

//   char message[MAX_CHARS_FOR_MESSAGE];
//   sprintf(message,"MyConfig::Section - section %s not found", name);
//   throw new MyException(5, message);

  return NULL;
}

void
MyConfig::Add(char *name)
{
  // check to see if we have this key
  MySection  *tmp;

  MySectionListIter li(_list);
  while(li){
    tmp = li.current();
    if(strcmp(tmp->Name(),name)==0)
      return;
    li++;
  }

  tmp = new MySection( name );
  _list.add(tmp);
  _number++;
}

void
MyConfig::Remove(char *name)
{
  // check to see if we have this key
  MySection  *tmp;

  MySectionListIter li(_list);
  while(li){
    tmp = li.current();
    if(strcmp(tmp->Name(),name)==0){
      // we have found the key
      // we remove it from the list
      _list.remove(tmp);
      // we delete the key data
      delete tmp;
      // decrement number of keys
      _number--;
      return;
    }
    li++;
  }
}

int
MyConfig::Number()
{
  return _number;
}

char *
MyConfig::Name() const
{
  return _name;
}

// the most important function:
// it realises the parsing of the config
void
MyConfig::Read()
{
  int lineNr = 0;
  char buff[MAX_CHARS_FOR_STRING];
  char tmp1[MAX_CHARS_FOR_STRING];
  char tmp2[MAX_CHARS_FOR_STRING];
  char section[MAX_CHARS_FOR_STRING] = "";
  char *p;
  char message[MAX_CHARS_FOR_MESSAGE];
  iconv_t direct = (iconv_t)-1, reverse = (iconv_t)-1;

  ifstream in(_name);
  while(1){
    in.getline(buff,MAX_CHARS_FOR_STRING);
    lineNr++;
    // comments support
    // ignore line if it starts with # or ;
    if(buff[0]=='#' || buff[0]==';')
      continue;

    if(buff[0]=='['){
      // it is a section
      p = strchr(buff,']');
      if(p && p > buff+1 ){
        *p = '\0';
        sprintf(section,"%s",buff+1);
        Add(section);
      }else{
        // we didn't find the delimiter ']'
        sprintf(message,
                "MyConfig::Read [%s / line %d] didn't find the ']'",
                _name, lineNr);
        throw new MyException(1,message);
      }
    }else{
      p = strrchr(buff,'=');
      if(p && p > buff+1 ){
        // it is a key
        *p = '\0';
        if(strcmp(section,"")){
          if(!strcmp(buff,"directEncoding")){
            direct = iconv_open("UTF-8", p+1);
          }else if(!strcmp(buff,"reverseEncoding")){
            reverse = iconv_open("UTF-8", p+1);
          }else{
            char *buff_key = buff, *buff_val = p+1;
            if(direct!=(iconv_t)-1 && strcmp(section,"Application")){
              char *inbuf = buff_key, *outbuf = tmp1;
              size_t inbytes = strlen(buff_key), outbytes = 255;
              iconv(direct,&inbuf,&inbytes,&outbuf,&outbytes);
              iconv(direct,NULL,NULL,NULL,NULL);
              *outbuf = '\0';
              buff_key = tmp1;
            }
            if(reverse!=(iconv_t)-1 && strcmp(section,"Application")){
              char *inbuf = buff_val, *outbuf = tmp2;
              size_t inbytes = strlen(buff_val), outbytes = 255;
              iconv(reverse,&inbuf,&inbytes,&outbuf,&outbytes);
              iconv(reverse,NULL,NULL,NULL,NULL);
              *outbuf = '\0';
              buff_val = tmp2;
            }
            Section(section)->Add(buff_key);
            Section(section)->Key(buff_key)->Set(buff_val);
          }
        }else{
          // no section defined
          sprintf(message,
                  "MyConfig::Read [%s / line %d] key with no section",
                  _name, lineNr);
          throw new MyException(2,message);
        }
      }
    }

    if (in.eof())
      return;
    if (!in.good()){
      sprintf(message,
              "MyConfig::Read [%s / line %d] error in imput stream",
              _name, lineNr);
      throw new MyException(3,message);
    }
  }

  if(direct!=(iconv_t)-1)
    iconv_close(direct);
  if(reverse!=(iconv_t)-1)
    iconv_close(reverse);
}

