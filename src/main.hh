/*
 * ===========================
 * langdrill: Language Drills
 * Version 0.1.7
 * 1/1999
 * ===========================
 *
 * Copyright (C) 1998, Ionutz Borcoman
 * Developed by Ionutz Borcoman <borco@usa.net>, <borco@borco-ei.eng.hokudai.ac.jp>
 *
 * Many thanks to Mario Motta <mmotta@guest.net> for the help given
 * during developing this program.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __main_hh__
#define __main_hh__

#include <fstream.h>
#include <unistd.h>
#include <time.h>

#include <vdk/vdk.h>

#include "config.hh"

#define ESC 27
#define MAX_TITLE_LEN 255
#define MAX_FONT_LEN 255
#define MAX_SENSE_NAME 255

typedef VDKCustomButton * PVDKCustomButton;

typedef struct {
  char name[PATH_MAX];
  int  sense; // sense of translation (eng->jap/jap->eng)
  char directSenseName[MAX_SENSE_NAME];
  char reverseSenseName[MAX_SENSE_NAME];
  char quizzAnswerNr[3]; // number of possible answers
  char hQuizz[4];
  char wQuizz[4];
  int  lessonNr;
  char **lessonTitle;
  char compatVersion[10];
  char timer[4];
  char limitedAnswerNr[4];
  int  limitedAnswers;
  int  useTimer;
  int  hideWindow;
} MyLangSetup;

//////////////////////////////
// The Main Form
//////////////////////////////

class MyLangForm: public VDKForm
{
  VDKBox				*_quizzBox;

  VDKCustomList			*_clist;

  VDKCustomList			*_clistWords;
  VDKColor				*_backgroundWords;
  bool					_clistWordsUpdated;

  VDKNumericEntry		*_quizzAnswerNrEntry;

  VDKFrame				*_quizzFrame;
  VDKLabel				*_quizzLabel;			// shows the quizz

  VDKCustomButton		*_loadButton;
  VDKCustomButton		*_reloadButton;
  VDKCustomButton		*_startButton;

  VDKCustomButton		**_quizzAnswerButton;	// shows the answers
  int					*_slot; 	// store handles of dynamic added signals

  VDKNotebook			*_book;

  VDKProgressBar		*_pbar;
  VDKRadioButtonGroup	*_senseGroup;
  VDKStatusbar			*_sbar;

  VDKTimer				*_timer;
  VDKNumericEntry		*_timerEntry;
  VDKProgressBar		*_timerBar;
  int					_elapsedTime;
  int					_timerCicle;
  bool					_timerRunning;

  VDKNumericEntry		*_answerStepEntry;
  VDKCheckButton		*_answerStepCheck;
  VDKCheckButton		*_hideCheck;
  VDKCheckButton		*_timerCheck;

  VDKFrame				*_simpleQuizzFrame;
  VDKTextView				*_simpleQuizzText;
  VDKLabel				*_simpleQuizzLabel;
  VDKCustomButton		*_simpleQuizzButton;
  MyKey					*_simpleQuizzKey;
  int					_simpleQuizzSense;

  // Menus entries
  VDKMenuItem *_exitMenu;
  VDKMenuItem *_aboutMenu;

  MyLangSetup _setup;

  MyConfig *_config;	// configuration tree

  int   *_argc;
  char **_argv;

  int _currentLesson;

  int _goodAnswers;
  int _goodTag;
  int _quizzes;

  bool changeAnswerButtons();
  void checkSections();
  void clearLessons();
  void distribute(int *array, int nr, int nr_avail);
  bool findLangDrillRC();
  void hideOrIconify();
  void loadLangDrillRC(bool isNew, char *tempFile);
  void newRandowSet(int rows, int cols, int *set);
  bool parseLangDrillRC( MyConfig *tempConfig, MyLangSetup &tempSetup );
  void setActiveButtons(int keyTotalNr);
  void showQuizz();
  int  useTimer();

public:
  MyLangForm(VDKApplication *app, int *argc, char *argv[]);
  ~MyLangForm();

  // predefined VDK functions
  bool CanClose();
  void Setup();

  // convenience functions
  void QuizzDestroy(int newNr, int oldNr);
  void QuizzSetup(int newNr, int oldNr, int w, int h);

  // GUI functions (interaction with user)
  bool AboutClicked( VDKObject* );
  bool ExitClicked( VDKObject* );
  bool LoadClicked( VDKObject* );
  bool QuizzAnswerClicked( VDKObject* );
  bool QuizzTextKeyPress( VDKObject *, GdkEvent *);
  bool ReloadClicked( VDKObject* );
  bool SelectRow( VDKObject* );
  bool SenseToggled( VDKObject* );
  bool SwitchPage( VDKObject* );
  bool StartClicked( VDKObject* );
  bool TimerClicked( VDKObject* );
  bool TimerTick( VDKObject* );

  DECLARE_SIGNAL_MAP(MyLangForm);
  DECLARE_SIGNAL_LIST(MyLangForm);
  DECLARE_EVENT_LIST(MyLangForm);
//   DECLARE_EVENT_MAP(MyLangForm);
};

#endif
