/*
 * ===========================
 * langdrill: Language Drills
 * Version 0.1.5
 * 12/1998
 * ===========================
 *
 * Copyright (C) 1998, Ionutz Borcoman
 * Developed by Ionutz Borcoman <borco@usa.net>, <borco@borco-ei.eng.hokudai.ac.jp>
 *
 * Many thanks to Mario Motta <mmotta@guest.net> for the help given
 * during developing this program.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __config__hh__
#define __config__hh__

#include <stdio.h>
#include <string.h>
#include <fstream.h>
#include <stdlib.h>
#include <vdk/dlist.h>

#define MAX_CHARS_FOR_NUMBER	10
#define MAX_CHARS_FOR_STRING	255
#define MAX_CHARS_FOR_MESSAGE	255

#define eol '\n'

class MyException;
class MyKey;
class MySection;

typedef VDKList<MyKey>             MyKeyList;
typedef VDKListiterator<MyKey>     MyKeyListIter;

typedef VDKList<MySection>         MySectionList;
typedef VDKListiterator<MySection> MySectionListIter;

/////////////////////////////////////////////
//
// strdup_new:
//
//		creates acopy of buff using op new.
//		returns a pointer to the new allocated
//		memory region. use delete to free
//		the memory.
//
/////////////////////////////////////////////
char *strdup_new(char * buff);

/////////////////////////////////////////////
// MyException:
/////////////////////////////////////////////
class MyException
{
  int   _value;
  char *_message;
public:
  MyException(int value, char *message);
  ~MyException();
  int Value();
  char *Message() const;
};

/////////////////////////////////////////////
//
// MyKey:
//
//		has a private buffer _value. can return
//		a copy of it or change its value.
//		convenient function for int values are
//		also provided.
//
/////////////////////////////////////////////
class MyKey
{
  char  *_value;
  char  *_name;
public:
  MyKey(char *name, char *value);
  MyKey(char *name, int value=0);
  ~MyKey();

  // returns a pointer to the _value
  char *Get() const;
  void  Set(char * value);

  // convenience functions
  int  GetInt();
  void SetInt(int value);

  char *Name() const;
};

/////////////////////////////////////////////
// MySection
/////////////////////////////////////////////
class MySection
{
  char   *_name;
  int     _number;
  MyKeyList _list;
public:
  MySection(char *name);
  ~MySection();

  void Add(char *name);
  void Remove(char *name);

  // this will return a pointer to key
  MyKey *Key(char *name);

  int Number();
  char *Name() const;
  MyKey *operator[](int n){
	return _list[n];
  }
};

/////////////////////////////////////////////
// MyConfigFile:
/////////////////////////////////////////////
class MyConfig
{
  char    *_name;
  int      _number;	// number of sections
  MySectionList _list;
public:
  MyConfig(char *file);
  ~MyConfig();

  void Add(char *name);
  void Remove(char *name);

  MySection *Section(char *name);

  int  Number();
  char *Name()const;

  void Read();
  void Write( char *name);

  MySection *operator[](int n){
	return _list[n];
  }
};

#endif
